# Getting started with Django on Google Cloud Platform on App Engine Standard

### Create an .env file in the project root directory.

```
# Paste the same contents as the line below to the file.

DEBUG=True
SECRET_KEY=bbm68+^_f#%c%^xrnhnb&q2gu2ync&%%-0z*)^gyzw2c#xm39v
GS_BUCKET=your-bucket-name
DB_NAME=dev
DB_USER=postgres
DB_PASSWORD=passw0rd
DB_HOST=127.0.0.1
```