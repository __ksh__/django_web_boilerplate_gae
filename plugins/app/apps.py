from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class Config(AppConfig):
    label = 'app'
    name = 'plugins.app'
    verbose_name = _('application')
