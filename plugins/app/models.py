from django.db import models
from django.utils.translation import gettext_lazy as _


class Robot(models.Model):
    name = models.CharField(_('name'), max_length=32)
    lang = models.CharField(_('lang'), max_length=32)

    class Meta:
        verbose_name = _('robot')
        verbose_name_plural = _('robots')

    def speak(self):
        return f"{self.name} speaks {self.lang}."
