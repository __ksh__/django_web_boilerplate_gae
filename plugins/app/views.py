import logging

from django.shortcuts import render
from django.views.generic import View


logger = logging.getLogger(__name__)


class BaseView(View):
    context = {}


class IndexView(BaseView):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.context)
