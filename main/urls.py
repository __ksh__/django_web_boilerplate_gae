from django.conf import settings
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(('plugins.app.urls', 'app')))
]

if settings.DEBUG:
    urlpatterns += [
        path('admin/doc/', include('django.contrib.admindocs.urls'))]
